@extends('layouts.layout')

@section('title')
    Les billets du blog
@endsection

@section('page_contenu')

@foreach($billets as $billet)
        <a href="{{ route('commentaire.show')}}"><h1 class="titreBillet">{{$billet->BIL_Titre}}</h1><a>
    </br>
        {{$billet->BIL_Date}}
    </br>
        {{$billet->BIL_Contenu}}
    @endforeach

@endsection