<!DOCTYPE html>
<html lang="fr">
   <head>
        <title>@yield('page_titre')</title>
        <!-- Implémentation de bootstrap -->
        <!--------------------------------->
        <link rel="stylesheet" href="{{asset('/css/style.css')}}" />


    </head>
   <body>
        <div id="global">
        <h1 id="titreBlog">Tous les billets</h1>


        <!-- Contenu -->
        @yield('page_contenu')
        <!------------->

        <br>
        <br>
    
        <!-- Footer -->
        <div id="piedBlog">
            Monblog - copyright 3A Info - 2022
        </div>
</div>
    </body> 
</html>